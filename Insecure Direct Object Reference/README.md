### Insecure Direct Object Reference
An Insecure Direct Object Reference is a type of vulnerability when a programmer uses an identifier to directly access an internal implementation object but does not provide any additional authorization check or any additional access control. For example, If a user can change a URL transaction through client-side to view data of another transaction that is prohibited.

In 2013, OWASP top 10 vulnerabilities, this was ranked as number 4 since it is very dangerous.

A direct object reference vulnerability could happen very easily. Most programmers use IDs to refer to objects in database for example a user id could be called UserId and would be used as a primary key. If the web application uses the following URL

"https://www.example.com/transaction.php?id=74656"

An attacker could change id=74656 to id=74657 in the URL

The attack is explained in "Insecure Direct Object Reference file"