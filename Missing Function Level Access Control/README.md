### Missing Function level access control ### 
This type of vulnerability occurs when there are no proper authentication checks in the request handlers.  
Hence, attacker can craft and send malicious requests to the server.  
The vulnerabilities covered under this type are: SSRF and Web cache poisoning  
Vulnerable web pages, softwares and tools used to perform attacks are mentioned in documentation under section  
"Exploitation of Vulnerability" related to each type.
