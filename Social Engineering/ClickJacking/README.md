### Click Jacking ###
Clickjacking is the type of attack which tricks the user into clicking some link/button which looks like part of that site. 
But in the backend, the event is performed on the victim site.   
Three attacks are performed to provide the Proof of Concept:  
1. Demo of Likejacking  
2. Unintentional download of files  
3. Unintentional Transfer of money  
  
Attacker pages: index.html, index2.php  
Victim pages: facebook.html, download.html, transfer.php  
  
To perform first two attacks, simply load index.html in browser as attacker's page and perform attack.  
To perfrom the third attack:  
*xampp server is used to load web pages (.php files)   
*First login to banking site using login.php  
*Then load the index2.php and perform clickjacking 