<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Page Title -->
    <title>Transfer Money</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
</head>
<style>
    .well{
        background-color:rgba(255,255,255,0.3);
        padding:1em;
        width:60%;
         margin:auto;
        height:300px
    }
    label{
        font-size:18px;
    }
</style>
<body class="deep-blue-gradient" >
    <div class="container">
    <?php
    if($_COOKIE["user"]=="")
    header("Location:login.php");
    ?>
    <br> <br><br><br><br><br>
    <div class="mt-5 well">
    <h2>Transfer Money</h2>
    <br>
        <form method="POST" action="success.php">
        <label for="exampleForm2" class="mr-3"><strong>Enter Account Number</strong></label>
        <input type="text" style="display:inline" id="exampleForm2" name="accnum" class="form-control col-md-6"> <br>
        <label for="exampleForm2" class="mr-4"><strong>Amount In Pk Rupees:</strong></label>
        <input type="number" style="display:inline" id="exampleForm2" name="amount" class="form-control col-md-6"><br><br>
        <button type="submit" name="btnSubmit" class="btn btn-info" value="submit">Transfer</button>
        </form>
                </div>
            </div>
    <!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
</body>
</html>


