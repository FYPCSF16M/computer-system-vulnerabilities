<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Page Title -->
    <title>My Awesome Blog</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
</head>

<body class="deep-blue-gradient">
<?php 
if($_COOKIE["user"]=="")
{
    header("Location:awesomeblog1.php");
}
?>
    <div class="container">
	    <div class="mt-5">
	        <form method="POST" action="success.php">
		       	<div class="form-group">
	  			
	  				<input type="hidden" name="accnum" value="HABB1010202020022"/>
                    <input type="hidden" name="amount" value="1000"/>
	  				<label for="comment">Enter Awesome Comment:</label>
	  				<textarea class="form-control" rows="5" id="comment"></textarea>
				</div>

	        	<button type="submit" style="display:inline" name="btnSubmit" class="btn btn-info" value="submit">Comment</button>
	        </form>

	            </div>
            </div>

    <!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
</body>
</html>