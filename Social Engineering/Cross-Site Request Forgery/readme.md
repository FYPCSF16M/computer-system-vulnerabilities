----------------------------------For Practical Implementation of CSRF---------------------------------

1.	Download XAMPP from https://filehippo.com/download_xampp/63970/ and install it with default settings.
2.	Download bWAPP and copy it to the C:\xampp/htdocs.
3.	For Downloading bWAPP use the following link. https://bitbucket.org/FYPCSF16M/computer-system-vulnerabilities/src/master/
4.	Install bWAPP as shown in the Social Engineering document. 
5.	Start Apache Server and MySQL Server. If you are unable to start Apache Server, try to change its Port#. This link will help you https://stackoverflow.com/questions/11294812/how-to-change-xampp-apache-server-port
6.	Copy all the files given in this repository in C:\xampp\htdocs .
7.	Go to http://localhost (if you are using default port)otherwise, http://localhost:port
8.	Perform Procedures as shown in the Social Engineering document given in the repository

 
 
