### Unvalidated Redirects and Forwards ###

Unvalidated Redirects and Forwards is a vulnerability that occurs when an attacker is able to redirect a user to an untrusted website when the user clicks on a link that is located on the original or trusted website. It is also called an Open redirect vulnerability and can be dangerous. It could lead to phishing attacks or other social engineering attacks as well however it is not ranked as very dangerous.

The Attack and exploit is explained in "Unvalidated Redirects and Forwards file"