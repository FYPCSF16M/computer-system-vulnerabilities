<?php require('conn.php');  ?>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Page Title -->
    <title>Comment Section</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
</head>
<style>
    
</style>
<body class="deep-blue-gradient" >
    <div class="container">
    <div class="mt-5">
    <?php
        $cookie_name = "user";
        $cookie_value = "ABC XYZ";
        setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day

        if(isset($_GET["btnSubmit"]))
        {
            $user=$_REQUEST["user"];
            $title=$_REQUEST["_title"];
            $comment=$_REQUEST["comment"];
            $sql = "INSERT INTO comments VALUES('$user','$title','$comment')";
            if (mysqli_query($conn, $sql)) 
            {
                header("Location:XSS_comment.php");
            }
            else
            {
                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
            }
        }
    ?>
        <h4 class="mb-3">Comments</h4>
        <?php   
        $sql = "SELECT * FROM comments";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) { 
                // output data of each row
                  while($row = $result->fetch_assoc()) 
                  {
                  
                    ?>
                          <table class="table table-bordered">
                          <tr><td><b>Username: </b><?php echo $row["uname"] ?></td></tr>
                          <tr><td><b>Title: </b><?php echo $row["title"] ?></td></tr>
                          <tr><td><b>Comment: </b><?php echo $row["comment"] ?></td></tr>
                        </table>
                          <br><br>

                    <?php }  
                     
                  
                }
                else {
                  echo "No Comments";
                }?>
        

        <form method="GET">
            <label for="exampleForm2"><strong>Enter Username</strong></label><br>
            <input type="text" id="exampleForm2" name="user" class="form-control col-md-3">
            <br>
            <label for="exampleForm2"><strong>Enter Title</strong></label>
            <input type="text"  name="_title" class="form-control col-md-3">
            <div class="form-group">
                  <label for="comment">Comment:</label>
                  <textarea class="form-control" rows="5" name="comment"></textarea>
            </div>
            <button type="submit" style="display:inline" name="btnSubmit" class="btn btn-info" value="submit">Post</button>
            <br><br>
        </form>
    </div>
    </div>
    <!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
</body>
</html>
