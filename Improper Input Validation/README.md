### Improper Input Validation ###
Improper input validation or unchecked user input is a type of vulnerability in computer software that may be used for security exploits. When a software does not validate input properly, an attacker is able to craft the input in a form that is not expected by the rest of the application.   
  
Attacks performed on Improper input validation    
- SQL Injection (Details are given in Section # 2 of "Improper Input Validation" Documentation)   
- Cross-Site Scripting (Details are given in Section # 3 of "Improper Input Validation" Documentation)   
- Local File Inclusion (Details are given in Section # 4 of "Improper Input Validation" Documentation)  
- Path Traversal (Details are given in Section # 5 of "Improper Input Validation" Documentation)  
- OS Command Injection (Details are given in Section # 6 of "Improper Input Validation" Documentation)  
- Format String Vulnerability (Details are given in Section # 7 of "Improper Input Validation" Documentation)  
- Unrestricted File Upload (Details are given in Section # 8 of "Improper Input Validation" Documentation)  
- Php Code Injection (Details are given in Section # 9 of "Improper Input Validation" Documentation)  