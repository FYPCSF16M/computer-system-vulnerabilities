<?php require('conn.php');  ?>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Page Title -->
    <title>Banker Information</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
</head>
<style>
    
</style>
<body class="deep-blue-gradient" >
    <div class="container">
    <?php
    $id="";
    if(isset($_REQUEST["btnSubmit"])){
    $id = $_REQUEST["ID"];
}
    ?>
    <div class="mt-5">
        <form method="POST">
        <label for="exampleForm2"><strong>Enter Your ID</strong></label><br>
        <input type="text" style="display:inline" id="exampleForm2" name="ID" class="form-control col-md-3">
        <button type="submit" style="display:inline" name="btnSubmit" class="btn btn-info" value="submit">Search</button>
        </form>
    <table class="table table-bordered">
        <?php   
        $sql = "SELECT * FROM bankerinfo where ID='$id'";
         $result = $conn->query($sql);
        if ($result->num_rows > 0) { ?>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Branch Name</th>
            <th>City</th>
        </tr>
        <?php
                      // output data of each row
                  while($row = $result->fetch_assoc()) 
                  {
                  
                    ?>
                          <tr>
                          <td><?php echo $row["ID"] ?></td>
                          <td><?php echo $row["Name"] ?></td>
                          <td><?php echo $row["Email"] ?></td>
                          <td><?php echo $row["BranchName"] ?></td>
                          <td><?php echo $row["City"] ?></td>
                          </tr>

                      <?php }  
                     
                  
                }
                else {
                  echo "No record found!";
                }?>
        
    </table>
            </div>
            </div>
    <!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
</body>
</html>


