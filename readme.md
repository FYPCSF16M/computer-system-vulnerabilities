The computer system vulnerabilities are categorized as follows:  
1)Improper Input Validation  
2)Insecure Direct Object References  
3)Missing Function level access control  
4)Social Engineering  
5)Unvalidated Redirects and Forwards  
6)Wireless Network Vulnerabilities  
  
Within each directory, their types, attacks and proof of concepts are provided along with their required resources.